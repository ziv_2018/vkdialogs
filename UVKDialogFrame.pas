unit UVKDialogFrame;
{ base dialog frame
}
interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.Controls.Presentation, FMX.StdCtrls, FMX.Layouts, FMX.Effects,
  FMX.TextLayout,
  System.Generics.Collections;

type
  TVKDialogFrame = class(TFrame)
    rectBk: TRectangle;
    rectContainer: TRectangle;
    shad1: TShadowEffect;
    txtTitle: TText;
    procedure FormResize(Sender: TObject);
    procedure rectBkClick(Sender: TObject);
  protected
    FParentForm: TForm;
    FTextLayout : TTextLayout;

    FCloseEvent: TInputCloseDialogEvent;
    FCloseProc: TInputCloseDialogProc;

    procedure AsyncFree;
    function CalculateHeight(const T: TText; AWidth: single): single;
    procedure Resized; virtual;
    constructor Create(const AStartObj:TFmxObject;
        const ATitle:string;
        AResultEvent: TInputCloseDialogEvent); overload;
    constructor Create(const AStartObj:TFmxObject;
        const ATitle:string;
        AResultProc: TInputCloseDialogProc); overload;
  public
    class var FSelfRef:TVKDialogFrame;

    ModalResult : TModalResult;

    destructor Destroy; override;
  end;

var
  VKDialogFrame: TVKDialogFrame;

implementation


function GetParentForm(O:TFMXObject):TForm;
var P:TFmxObject;
begin
  p := o;
  repeat
    p:=p.Parent;
  until (p=nil) or (p is TForm);
  Result := p as TForm;
end;


{$R *.fmx}

constructor TVKDialogFrame.Create(const AStartObj: TFmxObject;
  const ATitle:string;
  AResultProc: TInputCloseDialogProc);
begin
  FCloseProc := AResultProc;
  Create(AStartObj, ATitle, NIL);
end;

constructor TVKDialogFrame.Create(const AStartObj:TFmxObject;
  const ATitle:string;
  AResultEvent: TInputCloseDialogEvent);
begin
  inherited Create(NIL);
  FSelfRef := Self;

  FTextLayout := TTextLayoutManager.DefaultTextLayout.Create(Canvas);
  txtTitle.Text := ATitle;

  FParentForm := GetParentForm(AStartObj);
  Parent := FParentForm;

  FCloseEvent := AResultEvent;

  if FParentForm<>NIL then
  begin
    SetBounds(0,0,FParentForm.ClientWidth,FParentForm.ClientHeight);
  end;
end;


destructor TVKDialogFrame.Destroy;
begin
  if Assigned(FCloseEvent) then
    FCloseEvent(Self, ModalResult);
  if Assigned(FCloseProc) then
    FCloseProc(ModalResult);
  FreeAndNil(FTextLayout);
  inherited Destroy;
end;

function TVKDialogFrame.CalculateHeight(const T: TText; AWidth: single): single;
begin
  if FTextLayout=nil then
    FTextLayout := TTextLayoutManager.DefaultTextLayout.Create(Canvas);

  FTextLayout.BeginUpdate;
  try
    FTextLayout.HorizontalAlign := t.TextSettings.HorzAlign;
    FTextLayout.VerticalAlign := t.TextSettings.VertAlign;
    FTextLayout.Font.Assign( t.Font );
    FTextLayout.MaxSize := TPointF.Create(AWidth, 10000);
    FTextLayout.Text := t.Text;
    FTextLayout.Trimming := t.Trimming;
    FTextLayout.WordWrap := t.WordWrap;
  finally
    FTextLayout.EndUpdate;
  end;

  // �������� ������ ������ ��� ������� ����������
  result := FTextLayout.Height;
end;


procedure TVKDialogFrame.FormResize(Sender: TObject);
begin
  Resized();
end;

procedure TVKDialogFrame.Resized; 
var
  r: TRect;
begin
  r := TRect.Create(0,0, Round(rectBk.Width),Round(rectBk.Height));
  r.Inflate(-r.width div 8, -r.height div 8);
  rectContainer.SetBounds(r.Left, r.Top, r.Width, r.Height);
end;

procedure TVKDialogFrame.AsyncFree;
begin
  TThread.ForceQueue(nil,
  procedure
  begin
    parent:=NIL;
    FreeAndNil(FSelfRef);
  end);
end;

procedure TVKDialogFrame.rectBkClick(Sender: TObject);
begin
  Parent := nil;
  ModalResult := 0;
  exit;
end;


end.
